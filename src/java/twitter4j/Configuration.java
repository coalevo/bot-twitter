/*
Copyright (c) 2007-2009, Yusuke Yamamoto
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Yusuke Yamamoto nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY Yusuke Yamamoto ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Yusuke Yamamoto BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package twitter4j;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com
 */
public class Configuration {

  private static String c_Source = "KaraNet";
  private static String c_ClientURL = "http://www.coalevo.net";

  public static String getCilentVersion() {
    return "TwitterBot 0.1.0";
  }//getCilentVersion

  public static String getCilentVersion(String clientVersion) {
    return clientVersion;
  }//getCilentVersion

  public static String getSource() {
    return c_Source;
  }//getSource

  public static void setSource(String source) {
    if (source == null) return;
    c_Source = source;
  }//getSource

  public static String getProxyHost() {
    return "";
  }//getProxyHost

  public static String getProxyHost(String proxyHost) {
    return proxyHost;
  }//getProxyHost

  public static String getProxyUser() {
    return "";
  }//getProxyUser

  public static String getProxyUser(String user) {
    return user;
  }//getProxyUser

  public static String getClientURL() {
    return c_ClientURL;
  }//getClientUrl

  public static void setClientURL(String clientURL) {
    if (clientURL == null) return;
    c_ClientURL = clientURL;
  }//setClientURL

  public static String getProxyPassword() {
    return "";
  }//getProxyPassword

  public static String getProxyPassword(String password) {
    return password;
  }//getProxyPassword

  public static int getProxyPort() {
    return 0;
  }//getProxyPassword

  public static int getProxyPort(int port) {
    return port;
  }

  public static int getConnectionTimeout() {
    return 20000;
  }//getConnectionTimeout

  public static int getConnectionTimeout(int connectionTimeout) {
    return connectionTimeout;
  }

  public static int getReadTimeout() {
    return 120000;
  }

  public static int getReadTimeout(int readTimeout) {
    return readTimeout;
  }

  public static int getRetryCount() {
    return 3;
  }

  public static int getRetryCount(int retryCount) {
    return retryCount;
  }

  public static int getRetryIntervalSecs() {
    return 10;
  }

  public static int getRetryIntervalSecs(int retryIntervalSecs) {
    return retryIntervalSecs;
  }

  public static String getUser() {
    return "";
  }

  public static String getUser(String userId) {
    return userId;
  }

  public static String getPassword() {
    return "";
  }

  public static String getPassword(String password) {
    return password;
  }

  public static String getUserAgent() {
    return "KaraNet Twitter Bot";
  }

  public static String getUserAgent(String userAgent) {
    return userAgent;
  }

  public static String getOAuthConsumerKey() {
    return "";
  }

  public static String getOAuthConsumerKey(String consumerKey) {
    return consumerKey;
  }

  public static String getOAuthConsumerSecret() {
    return "";
  }

  public static String getOAuthConsumerSecret(String consumerSecret) {
    return consumerSecret;
  }

  public static int getNumberOfAsyncThreads() {
    return 2;
  }

  public static boolean getDebug() {
    return false;
  }//getDebug

}//Configuration
