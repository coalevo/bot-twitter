/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.twitter.service;

/**
 * This is a tagging interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface TwitterBotConfiguration {

  public static String TWITTER_CACHESIZE_KEY = "twitter.cache.size";
  public static String TWITTER_SCANPERIOD_KEY = "twitter.scan.period";  
  public static String TWITTER_SOURCENAME_KEY = "twitter.client.source";
  public static String TWITTER_CLIENTURL_KEY = "twitter.client.url";

}//interface TwitterBotConfiguration
