/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.twitter.impl;

import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.UUIDGeneratorService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.system.service.ExecutionService;
import org.osgi.service.prefs.PreferencesService;

import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * This class...
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;

  private MessageResourceService m_MessageResourceService;
  private StatisticsService m_StatisticsService;
  private ExecutionService m_ExecutionService;
  private PreferencesService m_PreferencesService;
  private UUIDGeneratorService m_UUIDGeneratorService;

  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_StatisticsServiceLatch;
  private CountDownLatch m_ExecutionServiceLatch;
  private CountDownLatch m_PreferencesServiceLatch;
  private CountDownLatch m_UUIDGeneratorServiceLatch;

  private ConfigurationMediator m_ConfigMediator;

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public StatisticsService getStatisticsService(long wait) {
    try {
      if (wait < 0) {
        m_StatisticsServiceLatch.await();
      } else if (wait > 0) {
        m_StatisticsServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getStatisticsService()", e);
    }
    return m_StatisticsService;
  }//getStatisticsService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getExecutionService()", e);
    }
    return m_ExecutionService;
  }//getExecutionService

  public PreferencesService getPreferencesService(long wait) {
    try {
      if (wait < 0) {
        m_PreferencesServiceLatch.await();
      } else if (wait > 0) {
        m_PreferencesServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getPreferencesService()", e);
    }
    return m_PreferencesService;
  }//getPreferencesService

  public UUIDGeneratorService getUUIDGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_UUIDGeneratorServiceLatch.await();
      } else if (wait > 0) {
        m_UUIDGeneratorServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getUUIDGeneratorService()", e);
    }
    return m_UUIDGeneratorService;
  }//getUUIDGeneratorService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigMediator = configMediator;
  }//setConfigMediator

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_MessageResourceServiceLatch = createWaitLatch();
    m_StatisticsServiceLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();
    m_PreferencesServiceLatch = createWaitLatch();
    m_UUIDGeneratorServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(objectclass=" + MessageResourceService.class.getName() + ")" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + StatisticsService.class.getName() + "))" +
            "(objectclass=" + PreferencesService.class.getName() + "))" +
            "(objectclass=" + UUIDGeneratorService.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MessageResourceService = null;

    //Latches
    if(m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if(m_StatisticsServiceLatch != null) {
      m_StatisticsServiceLatch.countDown();
      m_StatisticsServiceLatch = null;
    }
    if(m_ExecutionServiceLatch != null) {
      m_ExecutionServiceLatch.countDown();
      m_ExecutionServiceLatch = null;
    }
    if(m_PreferencesServiceLatch != null) {
      m_PreferencesServiceLatch.countDown();
      m_PreferencesServiceLatch = null;
    }
    if(m_UUIDGeneratorServiceLatch != null) {
      m_UUIDGeneratorServiceLatch.countDown();
      m_UUIDGeneratorServiceLatch = null;
    }
    m_BundleContext = null;
    m_ConfigMediator = null;
    m_LogMarker = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = (StatisticsService) o;
            m_StatisticsServiceLatch.countDown();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
          } else if (o instanceof PreferencesService) {
            m_PreferencesService = (PreferencesService) o;
            m_PreferencesServiceLatch.countDown();
          } else if (o instanceof UUIDGeneratorService) {
            m_UUIDGeneratorService = (UUIDGeneratorService) o;
            m_UUIDGeneratorServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = null;
            m_StatisticsServiceLatch = createWaitLatch();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
          } else if (o instanceof PreferencesService) {
            m_PreferencesService = null;
            m_PreferencesServiceLatch = createWaitLatch();
          } else if (o instanceof UUIDGeneratorService) {
            m_UUIDGeneratorService = null;
            m_UUIDGeneratorServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;


}//class ServiceMediator
